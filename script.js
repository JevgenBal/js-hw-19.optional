function estimateProjectCompletion(teamSpeed, backlog, deadline) {
  const teamSize = teamSpeed.length;

  const totalTeamSpeed = teamSpeed.reduce((acc, speed) => acc + speed, 0);

  const totalPoints = backlog.reduce((acc, points) => acc + points, 0);
  const totalDays = Math.ceil(totalPoints / totalTeamSpeed);

  const today = new Date();

  let completionDate = new Date(today);
  completionDate.setDate(completionDate.getDate() + totalDays);

  if (completionDate <= deadline) {
    const daysRemaining = Math.ceil((deadline - today) / (1000 * 60 * 60 * 24));
    console.log(`Усі завдання будуть успішно виконані за ${daysRemaining} днів до настання дедлайну!`);
  } else {
    const hoursOverdue = Math.ceil((completionDate - deadline) / (1000 * 60 * 60));
    console.log(`Команді розробників доведеться витратити додатково ${hoursOverdue} годин після дедлайну, щоб виконати всі завдання в беклозі.`);
  }
}

const teamSpeed = [4, 6, 5]; // Швидкість роботи членів команди
const backlog = [10, 8, 6, 4]; // Завдання в беклозі
const deadline = new Date('2023-07-10'); // Дедлайн

estimateProjectCompletion(teamSpeed, backlog, deadline);
