function estimateProjectCompletion() {
  const teamSpeedInput = prompt("Введіть швидкість роботи членів команди (через кому):");
  const teamSpeed = teamSpeedInput.split(",").map(speed => parseInt(speed.trim(), 10));

  const backlogInput = prompt("Введіть завдання в беклозі (через кому):");
  const backlog = backlogInput.split(",").map(points => parseInt(points.trim(), 10));

  const deadlineInput = prompt("Введіть дату дедлайну (у форматі YYYY-MM-DD):");
  const deadline = new Date(deadlineInput);

  const teamSize = teamSpeed.length;

  const totalTeamSpeed = teamSpeed.reduce((acc, speed) => acc + speed, 0);

  const totalPoints = backlog.reduce((acc, points) => acc + points, 0);
  const totalDays = Math.ceil(totalPoints / totalTeamSpeed);

  const today = new Date();

  let completionDate = new Date(today);
  completionDate.setDate(completionDate.getDate() + totalDays);

  if (completionDate <= deadline) {
    const daysRemaining = Math.ceil((deadline - today) / (1000 * 60 * 60 * 24));
    alert(`Усі завдання будуть успішно виконані за ${daysRemaining} днів до настання дедлайну!`);
  } else {
    const hoursOverdue = Math.ceil((completionDate - deadline) / (1000 * 60 * 60));
    alert(`Команді розробників доведеться витратити додатково ${hoursOverdue} годин після дедлайну, щоб виконати всі завдання в беклозі.`);
  }
}

estimateProjectCompletion();